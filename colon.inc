;Макрос colon с двумя аргументами: ключом и меткой, которая будет сопоставлена значению.
;Эта метка не может быть сгенерирована из самого значения, так как в строчке могут быть символы, которые не могут встречаться в
; метках, например, арифметические знаки, знаки пунктуации и т.д. После использования такого макроса можно напрямую указать значение, 
;сопоставляемое ключу

%define last 0
%macro colon 2
	%2: dq last
		db %1, 0
	%define last %2
%endmacro
