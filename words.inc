%include "colon.inc"

section .rodata

colon "again", seventh_word
db "seven", 0

colon "you", sixth_word
db "six", 0

colon "see", fifth_word
db "five", 0

colon "to", fourth_word
db "four", 0

colon "nice", third_word
db "three", 0

colon "world", second_word
db "two", 0

colon "hello", first_word
db "one", 0
